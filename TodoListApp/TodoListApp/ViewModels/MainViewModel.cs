﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading.Tasks;
using TodoListAppShared.DbContexts;
using Xamarin.Forms;

namespace TodoListApp.ViewModels
{
    public class MainViewModel : INotifyPropertyChanged
    {
        private string _taskName;

        public string TaskName
        {
            get => _taskName;
            set
            {
                _taskName = value;
                OnPropertyChanged(nameof(TaskName));
                AddCommand.ChangeCanExecute();
            }
        }

        public Command AddCommand { get; }

        public Command ApperingCommnad { get; }

        public ObservableCollection<string> Tasks { get; } = new ObservableCollection<string>();

        private readonly IDatabaseContext _databaseContext;

        public MainViewModel(IDatabaseContext databaseContext)
        {
            AddCommand = new Command(async () => await OnAddCommandAsync(), () => !string.IsNullOrWhiteSpace(TaskName));
            ApperingCommnad = new Command(async () => await OnApperingCommnadAsync());

            _databaseContext = databaseContext;
        }

        private async Task OnApperingCommnadAsync()
        {
            await _databaseContext.CreateDbIfNotExistsAsync();

            Tasks.Clear();

            var tasks = await _databaseContext.GetTaskAsync();

            foreach (var task in tasks)
            {
                Tasks.Add(task);
            }
        }

        private async Task OnAddCommandAsync()
        {
            await _databaseContext.AddTaskAsync(TaskName);
            await OnApperingCommnadAsync();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
