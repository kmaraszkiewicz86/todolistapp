﻿using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.DependencyInjection;
using TodoListApp.ViewModels;
using TodoListAppShared.DbContexts;
using System.IO;

namespace TodoListApp.Extensions
{
    public static class HostingExtesnion
    {
        public static IServiceProvider ServiceProvider { get; private set; }

        public static void ConfigureDependencyInjection(string path)
        {
            var host = new HostBuilder()
                 .ConfigureHostConfiguration(config =>
                 {
                     config.AddCommandLine(
                        new[] { $"ContentRoot={path}" });
                 })
                 .ConfigureServices(services =>
                 {
                     services.AddScoped<IDatabaseContext>(s => new DatabaseContext(Path.Combine(path, "tasks-db.db3")));
                     services.AddScoped<MainViewModel>();
                     services.AddScoped<MainPage>();
                 });

            ServiceProvider = host.Build().Services;
        }
    }
}
