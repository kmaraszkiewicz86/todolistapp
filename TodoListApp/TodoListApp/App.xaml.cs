﻿using System;
using TodoListApp.Extensions;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TodoListApp
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            HostingExtesnion.ConfigureDependencyInjection(FileSystem.AppDataDirectory);

            MainPage = HostingExtesnion.ServiceProvider.GetService<MainPage>();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
