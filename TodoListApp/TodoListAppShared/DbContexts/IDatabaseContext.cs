﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SQLite;

namespace TodoListAppShared.DbContexts
{
    public interface IDatabaseContext
    {
        SQLiteAsyncConnection Connection { get; }

        Task AddTaskAsync(string taskName);
        Task CreateDbIfNotExistsAsync();
        Task<IEnumerable<string>> GetTaskAsync();
    }
}