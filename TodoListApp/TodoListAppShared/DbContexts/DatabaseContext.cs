﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SQLite;
using TodoListAppShared.Entities;
using System.Linq;

namespace TodoListAppShared.DbContexts
{
    public class DatabaseContext : IDatabaseContext
    {
        public SQLiteAsyncConnection Connection { get; }

        public DatabaseContext(string dbPath)
        {
            Connection = new SQLiteAsyncConnection(dbPath);
        }

        public async Task CreateDbIfNotExistsAsync()
        {
            await Connection.CreateTableAsync<PlanningTask>();
        }

        public async Task AddTaskAsync(string taskName)
        {
            await Connection.InsertAsync(new PlanningTask
            {
                Name = taskName
            });
        }

        public async Task<IEnumerable<string>> GetTaskAsync()
        {
            return (await Connection.Table<PlanningTask>()
                .ToListAsync())
                .Select(p => p.Name);
        }
    }
}
