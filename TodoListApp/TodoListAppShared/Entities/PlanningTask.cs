﻿using System;
using SQLite;

namespace TodoListAppShared.Entities
{
    public class PlanningTask
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        [NotNull]
        public string Name { get; set; }
    }
}
